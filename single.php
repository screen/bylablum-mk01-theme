<?php get_header('blog'); ?>
<?php the_post(); ?>
<div class="container-fluid p-0">
    <div class="row no-gutters">
        <div class="title-breadcrumb-container breadcrumb-single-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row no-gutters align-items-center">
                    <div class="main-title-container col-12 col-xl-4 col-lg-4 col-md-7 col-sm-12">
                        <h1><?php _e('Blog by La Blum', 'bylablum'); ?></h1>
                    </div>
                    <div class="breadcrumb-container col-12 col-xl-8 col-lg-8 col-md-5 col-sm-12">
                        <?php the_breadcrumb(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<main class="container-fluid">
    <div class="row">
        <div class="single-main-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
            <div class="container">
                <div class="row">
                    <?php /* GET THE POST FORMAT */ ?>
                    <?php get_template_part( 'post-formats/format', get_post_format() ); ?>
                    <aside class="the-sidebar col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12" role="complementary">
                        <?php get_sidebar(); ?>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
