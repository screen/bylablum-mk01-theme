<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-section page-about-hero col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row">
                    <div class="page-about-hero-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo get_post_meta(get_the_ID(), 'blb_hero_banner', true); ?>);" data-aos="fadeIn" data-aos-delay="100">
                        <div class="container">
                            <div class="row row-hero align-items-center">
                                <div class="hero-content col-12 col-xl-7 col-lg-7 col-md-7 col-sm-7" data-aos="fadeIn" data-aos-delay="500">
                                    <div class="about-hero-text">
                                        <img src="<?php echo get_post_meta(get_the_ID(), 'blb_hero_small_image', true); ?>" alt="<?php _e('Acerca de La Blum', 'bylablum'); ?>" class="img-fluid" data-aos="fadeIn" data-aos-delay="200" />
                                        <h2 data-aos="fadeIn" data-aos-delay="200">
                                            <?php echo apply_filters('the_content', get_post_meta( get_the_ID(), 'blb_hero_description', true )); ?>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="row">
        <section class="page-section home-menu-section d-none d-sm-none d-md-none d-lg-flex d-xl-flex col-xl-12 col-lg-12 col-md-12">
            <div class="container-fluid home-menu-section-content">
                <div class="row align-items-center justify-content-center">
                    <div class="home-menu-logo col-xl-1 col-lg-1 col-md-1 col-sm-1 col-2 hidden">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="" class="img-fluid img-invert" />
                    </div>
                    <div class="home-menu-content col-xl-10 col-lg-10 col-md-10 col-sm-10 col-12">
                        <?php
                        wp_nav_menu( array(
                            'theme_location'    => 'home_menu',
                            'depth'             => 0, // 1 = with dropdowns, 0 = no dropdowns.
                            'container'         => 'div',
                            'menu_class'        => 'custom-home-nav'
                        ) );
                        ?>
                    </div>
                    <div class="home-menu-bar col-xl-1 col-lg-1 col-md-1 col-sm-1 col-2 hidden">
                        <div id="menu_icon" class="menu-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="home-menu-extra home-menu-extra-hidden">
                    <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'header_menu',
                        'container'         => 'div'
                    ) );
                    ?>
                </div>
            </div>
        </section>
    </div>
    <div class="row">
        <?php /* INTRO SECTION */ ?>
        <div class="page-section page-about-intro-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row">
                    <div class="section-intro-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row justify-content-center">
                            <div class="section-intro-text col-xl-6 col-lg-8 col-md-10 col-sm-12 col-12">
                                <div class="page-about-intro-title-section">
                                    <img src="<?php echo get_post_meta(get_the_ID(), 'blb_intro_small_image', true); ?>" alt="<?php _e('Acerca de La Blum', 'bylablum'); ?>" class="img-fluid" data-aos="fadeIn" data-aos-delay="200" />
                                    <h2><?php echo get_post_meta( get_the_ID(), 'blb_intro_title', true ); ?></h2>
                                </div>
                                <div class="section-intro-text-content">
                                    <?php echo apply_filters( 'the_content', get_post_meta(get_the_ID(), 'blb_intro_description', true)); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php /* INTRO SECTION */ ?>
        <section class="page-section page-about-second-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid p-0">
                <div class="row no-gutters align-items-center">
                    <div class="section-about-item section-about-main-picture col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12" style="background: url(<?php echo get_post_meta(get_the_ID(), 'blb_about_main_banner', true); ?>);">

                    </div>
                    <div class="section-about-item col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="about-text-content">
                            <div class="about-text-title">
                                <img src="<?php echo get_post_meta(get_the_ID(), 'blb_about_small_image', true); ?>" alt="<?php _e('Acerca de La Blum', 'bylablum'); ?>" class="img-fluid" data-aos="fadeIn" data-aos-delay="200" />
                                <h2><?php echo get_post_meta( get_the_ID(), 'blb_about_title', true ); ?></h2>
                            </div>
                            <?php echo apply_filters( 'the_content', get_post_meta(get_the_ID(), 'blb_about_description', true)); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="row">
        <?php /* OBJECTIVES SECTION */ ?>
        <div class="page-section page-about-third-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row">
                    <div class="section-about-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row justify-content-center">
                            <div class="section-about-img col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <img src="<?php echo get_post_meta(get_the_ID(), 'blb_objective_small_image', true); ?>" alt="<?php _e('Acerca de La Blum', 'bylablum'); ?>" class="img-fluid" data-aos="fadeIn" data-aos-delay="200" />
                            </div>
                            <div class="section-about-text col-xl-6 col-lg-8 col-md-10 col-sm-12 col-12">
                                <?php echo apply_filters( 'the_content', get_post_meta(get_the_ID(), 'blb_objective_description', true)); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
