var scrollHeight = 0,
    lastScrollTop = 0,
    st = jQuery(document).scrollTop(),
    secondaryNav = jQuery('.home-menu-section-content'),
    secondaryNavTopPosition = secondaryNav.offset().top,
    flag = false,
    currentQuiz = 1,
    lastQuiz = 0,
    quizHandler = '',
    quizIdHandler = '',
    lastQuizHandler = '',
    input_checked = false,
    passd = true,
    testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

jQuery(document).ready(function ($) {
    "use strict";

    jQuery(window).on('scroll', function () {
        //        st > lastScrollTop ? jQuery(".floating-nav").addClass("is-hidden") : jQuery(window).scrollTop() > 200 ? (jQuery(".floating-nav").removeClass("is-hidden"), setTimeout(function () {}, 200)) : jQuery(".floating-nav").addClass("is-hidden"), lastScrollTop = st, 0 == jQuery(this).scrollTop() && jQuery(".floating-nav").addClass("is-hidden");

        if (jQuery(window).scrollTop() > 10) {
            jQuery('.header-mobile').addClass('fixed-mobile');
        } else {
            jQuery('.header-mobile').removeClass('fixed-mobile');
        }

        if (jQuery(window).scrollTop() > secondaryNavTopPosition) {
            secondaryNav.addClass('is-fixed');
            setTimeout(function () {
                jQuery('.home-menu-logo').removeClass('hidden');
                jQuery('.home-menu-bar').removeClass('hidden');
                jQuery('.home-menu-logo').addClass('slide-in-left');
                jQuery('.home-menu-bar').addClass('slide-in-right');
            }, 50);
        } else {
            secondaryNav.removeClass('is-fixed');
            setTimeout(function () {
                jQuery('.home-menu-logo').removeClass('slide-in-left');
                jQuery('.home-menu-bar').removeClass('slide-in-right');
                jQuery('.home-menu-logo').addClass('hidden');
                jQuery('.home-menu-bar').addClass('hidden');
            }, 50);
        }
    });

    jQuery('#menu_icon').on('click touchstart', function () {
        jQuery(this).toggleClass('open');
        jQuery('.home-menu-extra').toggleClass('home-menu-extra-hidden');
    });

    jQuery('#menu-btn-mobile').on('click touchstart', function () {
        if (!flag) {
            flag = true;
            jQuery(this).toggleClass('open');
            jQuery('.navbar-mobile-collapse').toggleClass('navbar-mobile-collapse-hidden');
            jQuery('.header-mobile').toggleClass('fixed-click-mobile');
            setTimeout(function () {
                flag = false;
            }, 300);
        }
    });



    jQuery('#search_opener').on('click touchstart', function () {
        jQuery('.search-container').removeClass('search-container-hidden');
    });

    jQuery('#search_closer').on('click touchstart', function () {
        jQuery('.search-container').addClass('search-container-hidden');
    });

    jQuery('#startQuiz').on('click touchstart', function () {
        jQuery('.section-quiz-content-first-content').html('<div class="loader"><div id="img1" class="img"></div><div id="img2" class="img"></div><div id="img3" class="img"></div><div id="img4" class="img"></div><div id="img5" class="img"></div></div>');
        setTimeout(function () {
            jQuery('.section-quiz-content-first-content').html('');
            jQuery('.quiz-container').removeClass('quiz-container-hidden');
        }, 2000);

    });

    /* QUIZ FORM CONTROLLER */
    if (jQuery('.quiz-container').length) {
        lastQuizHandler = jQuery('.quiz-container form article:last').prev('article').attr('id');
        quizHandler = lastQuizHandler.split('-');
        lastQuiz = parseInt(quizHandler[1]);




        jQuery('.quiz-content input[type=radio]').change(function () {
            "use strict";
            jQuery(this).addClass('radio-option-clicked');
        });

        jQuery('#quiz-next').on('click touchstart', function (e) {
            "use strict";
            quizIdHandler = jQuery('.active-quiz').attr('id');

            jQuery('#' + quizIdHandler + ' input.radio-option-input').each(function () {
                if (jQuery(this).is(':checked')) {
                    input_checked = true;
                }
            });

            if (!input_checked) {
                jQuery('.quiz-error-handler').removeClass('quiz-error-handler-hidden');
            } else {
                jQuery('.quiz-error-handler').addClass('quiz-error-handler-hidden');
                if (currentQuiz === 1) {
                    jQuery('.quiz-back').removeClass('quiz-button-hidden');
                }
                jQuery('#quiz-' + currentQuiz).removeClass('active-quiz');
                jQuery('#quiz-' + currentQuiz).addClass('hidden-quiz');
                currentQuiz = currentQuiz + 1;
                jQuery('#quiz-' + currentQuiz).removeClass('hidden-quiz');
                jQuery('#quiz-' + currentQuiz).addClass('active-quiz');
                jQuery('.quiz-counter span:first-child').html(currentQuiz);

                if (currentQuiz === lastQuiz) {
                    jQuery('.quiz-next').addClass('quiz-button-hidden');
                } else {
                    jQuery('.quiz-next').removeClass('quiz-button-hidden');
                }
            }
            input_checked = false;
        });

        jQuery('#quiz-back').on('click touchstart', function (e) {
            "use strict";
            jQuery('#quiz-' + currentQuiz).removeClass('active-quiz');
            jQuery('#quiz-' + currentQuiz).addClass('hidden-quiz');
            currentQuiz = currentQuiz - 1;
            jQuery('#quiz-' + currentQuiz).removeClass('hidden-quiz');
            jQuery('#quiz-' + currentQuiz).addClass('active-quiz');
            if (currentQuiz === 1) {
                jQuery('.quiz-back').addClass('quiz-button-hidden');
            }
            jQuery('.quiz-counter span:first-child').html(currentQuiz);
            if (currentQuiz < lastQuiz) {
                jQuery('.quiz-next').removeClass('quiz-button-hidden');
            }
        });

        jQuery('#' + lastQuizHandler + ' input.radio-option-input').change(function () {
            "use strict";
            setTimeout(function () {
                jQuery('#' + lastQuizHandler).removeClass('active-quiz');
                jQuery('#' + lastQuizHandler).addClass('hidden-quiz');
                jQuery('#quiz-mail').removeClass('hidden-quiz');
                jQuery('#quiz-mail').addClass('active-quiz');
            }, 600);
        });

        jQuery('#quiz-form').on('submit', function (e) {
            passd = true;

            var $el = jQuery('input[type=text]');
            if (($el.val() == '') || ($el.val() == null) || ($el.val().length < 2)) {
                $el.next("small").removeClass("d-none");
                $el.next("small").html("El campo no puede estar vacio ni tener menos de dos caracteres");
                passd = false;
            } else {
                if (!$el.val().match(/^[a-zA-Z ]+$/)) {
                    $el.next("small").removeClass("d-none");
                    $el.next("small").html("<strong>Error:</strong>Solo se permiten caracteres alfabéticos");
                    passd = false;
                } else {
                    $el.next("small").addClass("d-none");
                }
            }

            var $el = jQuery('input[type=email]');
            if (($el.val() == '') || ($el.val() == null) || ($el.val().length < 2)) {
                $el.next("small").removeClass("d-none");
                $el.next("small").html("El campo no puede estar vacio ni tener menos de dos caracteres");
                passd = false;
            } else {
                if (testEmail.test($el.val())) {
                    $el.next("small").addClass("d-none");
                } else {
                    $el.next("small").removeClass("d-none");
                    $el.next("small").html("El correo es invalido. recuerde que debe colocar un correo válido");
                    passd = false;
                }

            }

            if (passd == true) {
                jQuery.ajax({
                    type: 'POST',
                    url: admin_url.ajax_url,
                    data: {
                        action: 'bylablum_ajax_quiz_reporter',
                        info: jQuery('#quiz-form').serialize(),
                    },
                    beforeSend: function () {
                        jQuery('.quiz-mail-loader').removeClass('quiz-mail-loader-hidden');
                    },
                    success: function (response) {
                        var respuesta = jQuery.parseJSON(response);
                        jQuery('.quiz-mail-loader').html('<div class="quiz-mail-response">' + respuesta['message'] + '</div');
                        setTimeout(function () {
                            window.location = respuesta['url'];
                        }, 2000);

                    },
                    error: function (request, status, error) {
                        console.log(error);
                    }
                });
            }
            e.preventDefault();
        });

    }

    jQuery('.mailchimp_form').on('submit', function (event) {
        event.preventDefault();
        passd = true;
        var longid = jQuery(this).attr('id');
        var shortid = longid.split('_');

        var $el = jQuery('#FNAME');
        if (($el.val() == '') || ($el.val() == null) || ($el.val().length < 2)) {
            $el.next("small").removeClass("d-none");
            $el.next("small").html("El campo no puede estar vacio ni tener menos de dos caracteres");
            passd = false;
        } else {
            if (!$el.val().match(/^[a-zA-Z ]+$/)) {
                $el.next("small").removeClass("d-none");
                $el.next("small").html("<strong>Error:</strong>Solo se permiten caracteres alfabéticos");
                passd = false;
            } else {
                $el.next("small").addClass("d-none");
            }
        }

        var $el = jQuery('#EMAIL');
        if (($el.val() == '') || ($el.val() == null) || ($el.val().length < 2)) {
            $el.next("small").removeClass("d-none");
            $el.next("small").html("El campo no puede estar vacio ni tener menos de dos caracteres");
            passd = false;
        } else {
            if (testEmail.test($el.val())) {
                $el.next("small").addClass("d-none");
            } else {
                $el.next("small").removeClass("d-none");
                $el.next("small").html("El correo es invalido. recuerde que debe colocar un correo válido");
                passd = false;
            }

        }

        if (passd == true) {
            jQuery.ajax({
                type: 'POST',
                url: admin_url.ajax_url,
                data: {
                    action: 'bylablum_custom_ajax_form_add_susbscriber',
                    info: jQuery('#' + longid).serialize(),
                },
                beforeSend: function () {
                    jQuery('#response_' + shortid[2]).html('<div class="quiz-mail-loader"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>');
                },
                success: function (data) {
                    var respuesta = jQuery.parseJSON(data);
                    jQuery('#response_' + shortid[2]).html('<div class="form-response-wrapper">' + respuesta['message'] + '</div>');
                    SaveToDisk(jQuery('#file_download').val(), 'Descarga');
                },
                error: function (request, status, error) {
                    console.log(error);
                }
            });
        }
        passd == false;
    });



}); /* end of as page load scripts */

AOS.init({
    // Global settings
    disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
    startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
    initClassName: 'aos-init', // class applied after initialization
    animatedClassName: 'aos-animate', // class applied on animation
    useClassNames: false, // if true, will add content of `data-aos` as classes on scroll

    // Settings that can be overriden on per-element basis, by `data-aos-*` attributes:
    offset: 120, // offset (in px) from the original trigger point
    delay: 0, // values from 0 to 3000, with step 50ms
    duration: 400, // values from 0 to 3000, with step 50ms
    easing: 'ease', // default easing for AOS animations
    once: false, // whether animation should happen only once - while scrolling down
    mirror: false, // whether elements should animate out while scrolling past them
    anchorPlacement: 'top-bottom' // defines which position of the element regarding to window should trigger the animation
});

function SaveToDisk(fileURL, fileName) {
    // for non-IE
    if (!window.ActiveXObject) {
        var save = document.createElement('a');
        save.href = fileURL;
        save.download = fileName || 'unknown';
        save.style = 'display:none;opacity:0;color:transparent;';
        (document.body || document.documentElement).appendChild(save);

        if (typeof save.click === 'function') {
            save.click();
        } else {
            save.target = '_blank';
            var event = document.createEvent('Event');
            event.initEvent('click', true, true);
            save.dispatchEvent(event);
        }

        (window.URL || window.webkitURL).revokeObjectURL(save.href);
    }

    // for IE
    else if (!!window.ActiveXObject && document.execCommand) {
        var _window = window.open(fileURL, '_blank');
        _window.document.close();
        _window.document.execCommand('SaveAs', true, fileName || fileURL)
        _window.close();
    }
}
