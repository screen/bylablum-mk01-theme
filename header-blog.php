<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <?php /* MAIN STUFF */ ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
        <meta name="robots" content="NOODP, INDEX, FOLLOW" />
        <meta name="HandheldFriendly" content="True" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="dns-prefetch" href="//connect.facebook.net" />
        <link rel="dns-prefetch" href="//facebook.com" />
        <link rel="dns-prefetch" href="//googleads.g.doubleclick.net" />
        <link rel="dns-prefetch" href="//pagead2.googlesyndication.com" />
        <link rel="dns-prefetch" href="//google-analytics.com" />
        <?php /* FAVICONS */ ?>
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/images/favicon-16x16.png">
        <link rel="favicon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
        <?php /* THEME NAVBAR COLOR */ ?>
        <meta name="msapplication-TileColor" content="#603cba" />
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png" />

        <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/images/site.webmanifest">
        <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/images/safari-pinned-tab.svg" color="#545488">
        <meta name="theme-color" content="#ffffff">
        <?php /* AUTHOR INFORMATION */ ?>
        <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
        <meta name="author" content="ByLaBlum" />
        <meta name="copyright" content="https://bylablum.familiaperfecta.com/" />
        <meta name="DC.title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_the_title() . ' | ' . get_bloginfo('name'); } ?>" />
        <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
        <?php wp_title('|', false, 'right'); ?>
        <?php wp_head() ?>
        <?php /* OPEN GRAPHS INFO - COMMENTS SCRIPTS */ ?>
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php /* IE COMPATIBILITIES */ ?>
        <!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7" /><![endif]-->
        <!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8" /><![endif]-->
        <!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9" /><![endif]-->
        <!--[if gt IE 8]><!-->
        <html <?php language_attributes(); ?> class="no-js" />
        <!--<![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <!--[if IE]> <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> <![endif]-->
        <?php get_template_part('includes/fb-script'); ?>
        <?php get_template_part('includes/ga-script'); ?>
    </head>

    <body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NP7HD3J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div id="fb-root"></div>
        <header class="container-fluid p-0" role="banner" itemscope itemtype="http://schema.org/WPHeader">
            <div class="row no-gutters">
                <div class="the-header-blog d-none d-sm-none d-xl-block d-lg-block d-md-none col-xl-12 col-lg-12 col-md-12">
                    <div class="row align-items-center">
                        <div class="header-logo col-1">
                            <a class="navbar-brand" href="<?php echo home_url('/');?>" title="<?php echo get_bloginfo('name'); ?>">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="<?php echo get_bloginfo('name'); ?>" class="img-fluid img-brand img-invert" />
                            </a>
                        </div>
                        <div class="navbar-container col-10">
                            <nav class="the-navbar navbar navbar-expand-md" role="navigation">

                                <!-- Brand and toggle get grouped for better mobile display -->
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <?php
                                wp_nav_menu( array(
                                    'theme_location'    => 'home_menu',
                                    'depth'             => 1, // 1 = with dropdowns, 0 = no dropdowns.
                                    'container'         => 'div',
                                    'container_class'   => 'collapse navbar-collapse',
                                    'container_id'      => 'bs-example-navbar-collapse-1',
                                    'menu_class'        => 'navbar-nav mr-auto ml-auto',
                                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                                    'walker'            => new WP_Bootstrap_Navwalker()
                                ) );
                                ?>

                            </nav>
                        </div>
                        <div class="menu-hamburger col-1">
                            <div id="menu_icon" class="menu-icon">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                        <div class="home-menu-extra home-menu-blog-extra home-menu-extra-hidden">
                            <?php
                            wp_nav_menu( array(
                                'theme_location'    => 'header_menu',
                                'container'         => 'div'
                            ) );
                            ?>
                        </div>
                    </div>
                </div>
                <!-- MENU CONTAINER -->
                <nav class="floating-nav is-hidden menu_scroll_loader">
                    <div class="floating-nav__inner">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="the-header-blog d-none d-sm-none d-xl-block d-lg-block d-md-none col-xl-12 col-lg-12 col-md-12">
                                    <div class="row align-items-center">
                                        <div class="header-logo col-1">
                                            <a class="navbar-brand" href="<?php echo home_url('/');?>" title="<?php echo get_bloginfo('name'); ?>">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="<?php echo get_bloginfo('name'); ?>" class="img-fluid img-brand img-invert" />
                                            </a>
                                        </div>
                                        <div class="navbar-container col-10">
                                            <nav class="the-navbar navbar navbar-expand-md" role="navigation">

                                                <!-- Brand and toggle get grouped for better mobile display -->
                                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
                                                    <span class="navbar-toggler-icon"></span>
                                                </button>
                                                <?php
                                                wp_nav_menu( array(
                                                    'theme_location'    => 'home_menu',
                                                    'depth'             => 1, // 1 = with dropdowns, 0 = no dropdowns.
                                                    'container'         => 'div',
                                                    'container_class'   => 'collapse navbar-collapse',
                                                    'container_id'      => 'bs-example-navbar-collapse-1',
                                                    'menu_class'        => 'navbar-nav mr-auto ml-auto',
                                                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                                                    'walker'            => new WP_Bootstrap_Navwalker()
                                                ) );
                                                ?>

                                            </nav>
                                        </div>
                                        <div class="menu-hamburger col-1">
                                            <div id="menu_icon" class="menu-icon">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </div>
                                        </div>
                                        <div class="home-menu-extra home-menu-blog-extra home-menu-extra-hidden">
                                            <?php
                                            wp_nav_menu( array(
                                                'theme_location'    => 'header_menu',
                                                'container'         => 'div'
                                            ) );
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <div class="header-mobile header-blog-mobile col-12 col-sm-12 d-flex d-sm-flex d-md-flex d-lg-none d-xl-none">
                    <div class="row">
                        <nav class="the-navbar-mobile col-12" role="navigation">
                            <div id="menu-btn-mobile" class="menu-icon">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <a class="navbar-brand" href="<?php echo home_url('/');?>" title="<?php echo get_bloginfo('name'); ?>">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="<?php echo get_bloginfo('name'); ?>" class="img-fluid img-brand" />
                            </a>
                            <div class="navbar-search-icon">
                                <a id="search_opener"><i class="fa fa-search"></i></a>
                            </div>
                        </nav>
                    </div>
                    <div class="navbar-mobile-collapse navbar-mobile-collapse-hidden" id="navbarSupportedContent">
                        <div class="menu-menubar-container col-12">
                            <?php
                            wp_nav_menu( array(
                                'theme_location'    => 'header_menu',
                                'depth'             => 1, // 1 = with dropdowns, 0 = no dropdowns.
                                'container'         => 'ul',
                            ) );

                            wp_nav_menu( array(
                                'theme_location'    => 'home_menu',
                                'depth'             => 1, // 1 = with dropdowns, 0 = no dropdowns.
                                'container'         => 'ul',
                            ) );
                            ?>
                        </div>
                        <div class="search-menubar-container col-12">
                            <?php get_template_part('menu-searchform'); ?>
                        </div>
                        <div class="social-menubar-container col-12">
                            <a href="<?php echo get_option('bylablum_fb'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="<?php echo get_option('bylablum_tw'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="<?php echo get_option('bylablum_ig'); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a href="<?php echo get_option('bylablum_yt'); ?>" target="_blank"><i class="fa fa-youtube"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="search-container search-container-hidden">
            <div class="search-container-controller">
                <h2><?php _e('Buscar:', 'bylablum'); ?></h2>
                <a id="search_closer" class="search-closer">
                    <i class="fa fa-close"></i>
                </a>
            </div>

            <?php get_template_part('menu-searchform'); ?>
        </div>
