<?php get_header('blog'); ?>
<div class="container-fluid p-0">
    <div class="row no-gutters">
        <div class="title-breadcrumb-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row no-gutters align-items-center">
                    <div class="main-title-container col-12 col-xl-9 col-lg-9 col-md-7 col-sm-6">
                        <h1><?php single_cat_title(); ?></h1>
                    </div>
                    <div class="breadcrumb-container col-12 col-xl-3 col-lg-3 col-md-5 col-sm-6">
                        <?php the_breadcrumb(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <div class="page-container blog-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <?php if (have_posts()) : ?>
                    <section class="main-blog-container col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">
                        <div class="row">
                            <?php $defaultatts = array('class' => 'img-fluid', 'itemprop' => 'image'); ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <article id="post-<?php the_ID(); ?>" class="archive-item col-12 <?php echo join(' ', get_post_class()); ?>" role="article">
                                <div class="row">
                                    <picture class="archive-item-image col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <?php if ( has_post_thumbnail()) : ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                            <?php the_post_thumbnail('full', $defaultatts); ?>
                                        </a>
                                        <?php else : ?>
                                        <img itemprop="image" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/no-img.jpg" alt="No img" class="img-fluid" />
                                        <?php endif; ?>
                                        <header>
                                            <div class="category-container">
                                                <?php the_category(); ?>
                                            </div>
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                <h2 rel="bookmark" title="<?php the_title_attribute(); ?>">
                                                    <?php the_title(); ?>
                                                </h2>
                                            </a>
                                        </header>
                                    </picture>
                                    <div class="archive-item-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <div class="archive-item-meta col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="row align-items-center">
                                            <div class="archive-item-author col-9">
                                                <?php the_author(); ?>
                                            </div>
                                            <div class="archive-item-social col-3">
                                                <?php $link = urlencode(get_the_permalink()); ?>
                                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>" title="<?php _e('Compartir en Facebook', 'bylablum'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                                                <a href='https://twitter.com/share?url=<?php echo $link; ?>&text=<?php echo get_the_title(); ?>&via=blogbylablum&hashtags=bylablum' target="_blank"><i class="fa fa-twitter"></i></a>
                                                <a href="https://pinterest.com/pin/create/button/?url=<?php echo $link; ?>&media=<?php echo get_the_post_thumbnail_url('full'); ?>&description=<?php echo get_the_title(); ?>" target="_blank"><i class="fa fa-pinterest"></i></a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </article>
                            <?php endwhile; ?>
                            <div class="pagination col-12">
                                <?php if(function_exists('wp_paginate')) { wp_paginate(); } else { posts_nav_link(); wp_link_pages(); } ?>
                            </div>
                        </div>
                    </section>
                    <aside class="col-3 col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <?php get_sidebar(); ?>
                    </aside>
                    <?php else: ?>
                    <section>
                        <h2>
                            <?php _e('Disculpe, su busqueda no arrojo ningun resultado', 'bylablum'); ?>
                        </h2>
                        <h3>
                            <?php _e('Dirígete nuevamente al', 'bylablum'); ?> <a href="<?php echo home_url('/'); ?>" title="<?php _e('Volver al Inicio', 'bylablum'); ?>">
                            <?php _e('inicio', 'bylablum'); ?></a>.</h3>
                    </section>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
