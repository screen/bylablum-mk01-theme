<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid main-container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <div class="page-section quiz-section hero-result-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row">
                    <div class="section-quiz-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="section-quiz-content-first-content">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/result-icon.png" alt="Resultado" class="img-fluid" />
                        </div>
                    </div>
                </div>
            </div>
            <a href="#result">
                <img src="<?php echo get_template_directory_uri(); ?>/images/icon-down.png" alt="Click para bajar" class="img-fluid img-down" />
            </a>
        </div>
    </div>
    <div class="row">
        <div id="result" class="page-section result-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-center">
                    <div class="section-result-content col-xl-5 col-lg-8 col-md-10 col-sm-12 col-12">
                        <?php $quiz_id = $_GET['quiz-id']; ?>
                        <?php $score = $_GET['score']; ?>
                        <?php $quiz_active = get_post($quiz_id); ?>
                        <?php $quiz_options = (array)get_post_meta($quiz_id, 'quiz_options', true);
                        foreach ($quiz_options as $quiz_options_item) {
                            $quiz_score =  explode('-', $quiz_options_item['blb_quiz_range_score']);
                            if (((int)$quiz_score[0] <= $score) && ($score <= (int)$quiz_score[1])) {
                                $description = $quiz_options_item['blb_quiz_range_desc'];
                                $link = $quiz_options_item['blb_quiz_range_url'];
                            }
                        }?>
                        <?php echo apply_filters('the_content', $description); ?>
                        <a class="btn btn-md btn-green" href="<?php echo esc_url($link); ?>" target="_blank" title="<?php _e('equipate', 'bylablum'); ?>">
                            <?php _e('equipate', 'bylablum'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php $post = get_page_by_path( 'home' ); ?>
        <?php setup_postdata( $post ); ?>
        <?php $show_podcast = get_post_meta( get_the_ID(), 'blb_podcast_checkbox', true ); ?>
        <?php if (!$show_podcast == 'on') { ?>
        <section class="page-section section-podcast col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row">
                    <div class="section-podcast-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo get_post_meta(get_the_ID(), 'blb_podcast_banner', true); ?>);" data-aos="fadeIn" data-aos-delay="100">
                        <div class="container">
                            <div class="row row-podcast align-items-center">
                                <div class="podcast-hero col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
                                    <img src="<?php echo get_post_meta(get_the_ID(), 'blb_podcast_small_image', true); ?>" alt="<?php echo get_bloginfo('name')?>" class="img-fluid" data-aos="fadeIn" data-aos-delay="200" />
                                    <div class="podcast-button" data-aos="fadeIn" data-aos-delay="500">
                                        <a class="btn btn-md btn-gray" href="#">
                                            <?php _e('Escucha el Podcast', 'bylablum'); ?> <i class="fa fa-play"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-podcast-text-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row">
                            <div class="podcast-text-item podcast-text-item-first col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="250">
                                <?php $desc = get_post_meta( get_the_ID(), 'blb_podcast_description1', true ); ?>
                                <?php echo apply_filters( 'the_content', $desc ); ?>
                            </div>
                            <div class="podcast-text-item col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="300">
                                <?php $desc = get_post_meta( get_the_ID(), 'blb_podcast_description2', true ); ?>
                                <?php echo apply_filters( 'the_content', $desc ); ?>
                            </div>
                            <div class="podcast-text-item podcast-box-container col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="400">
                                <?php $args = array('post_type' => 'podcast', 'posts_per_page' => 6, 'orderby' => 'date', 'order' => 'DESC'); ?>
                                <?php $podcast_list = new WP_Query($args); ?>
                                <?php if ($podcast_list->have_posts()) : ?>
                                <ul class="podcast-box" data-aos="slideUp" data-aos-delay="200">
                                    <li class="podcast-heading">
                                        <h2>
                                            <?php _e('Escucha la lista de reproduccion', 'bylablum'); ?>
                                        </h2>
                                    </li>
                                    <?php while ($podcast_list->have_posts()) : $podcast_list->the_post(); ?>
                                    <li>
                                        <a href="<?php the_permalink(); ?>" title="<?php _e('Escuchar Podcast', 'bylablum'); ?>">
                                            <?php the_title(); ?> <i class="fa fa-play"></i></a>
                                    </li>
                                    <?php endwhile; ?>
                                </ul>
                                <?php endif; ?>
                                <?php wp_reset_query(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>
        <?php wp_reset_query(); ?>
    </div>
</main>
<?php get_footer(); ?>
