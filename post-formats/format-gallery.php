<?php /* POST FORMAT - DEFAULT */ ?>
<?php $defaultatts = array('class' => 'img-fluid', 'itemprop' => 'image'); ?>
<article id="post-<?php the_ID(); ?>" class="the-single col-9 col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 <?php echo join(' ', get_post_class()); ?>" itemscope itemtype="http://schema.org/Article">

    <header>
        <div class="category-container">
            <?php the_category(' '); // Separated by commas ?>
        </div>
        <h1 itemprop="name"><?php the_title(); ?></h1>
    </header>
    <div class="row">
        <div class="single-meta-container col-12">
            <div class="row">
                <div class="single-meta-info col-9">
                    <span class="date"><?php the_time('F j, Y'); ?></span>
                    <span class="author"><?php _e('por', 'bylablum'); ?> <?php the_author_posts_link(); ?></span>
                </div>
                <div class="single-meta-social col-3">
                      <!-- Your like button code -->   <div class="fb-like"      data-href="<?php the_permalink(); ?>"      data-layout="standard"      data-action="like"      data-show-faces="true">   </div>
                </div>
            </div>

        </div>
    </div>
    <?php if ( has_post_thumbnail()) : ?>
    <picture>
        <?php the_post_thumbnail('full', $defaultatts); ?>
    </picture>
    <?php endif; ?>
    <div class="post-content" itemprop="articleBody">
        <?php the_content() ?>
        <?php wp_link_pages( array(
    'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bylablum' ) . '</span>',
    'after'       => '</div>',
    'link_before' => '<span>',
    'link_after'  => '</span>', ) ); ?>

    </div><!-- .post-content -->
    <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
    <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
    <meta itemprop="url" content="<?php the_permalink() ?>">
    <?php if ( comments_open() ) { comments_template(); } ?>
</article> <?php // end article ?>
