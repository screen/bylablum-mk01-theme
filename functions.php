<?php
/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

require_once('includes/wp_enqueue_styles.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action('wp_enqueue_scripts', 'my_jquery_enqueue');
function my_jquery_enqueue() {
    wp_deregister_script('jquery');
    wp_deregister_script('jquery-migrate');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '3.3.1', false);
        /*- JQUERY MIGRATE ON LOCAL  -*/
        wp_register_script( 'jquery-migrate', get_template_directory_uri() . '/js/jquery-migrate.min.js',  array('jquery'), '3.0.1', false);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script( 'jquery', 'https://code.jquery.com/jquery-3.3.1.min.js', false, '3.3.1', false);
        /*- JQUERY MIGRATE ON WEB  -*/
        wp_register_script( 'jquery-migrate', 'https://code.jquery.com/jquery-migrate-3.0.1.min.js', array('jquery'), '3.0.1', true);
    }
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-migrate');
}

/* NOW ALL THE JS FILES */
require_once('includes/wp_enqueue_scripts.php');

/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

// WALKER COMPLETO TOMADO DESDE EL NAVBAR COLLAPSE
require_once('includes/class-wp-bootstrap-navwalker.php');

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD REQUIRED WORDPRESS PLUGINS
-------------------------------------------------------------- */

//require_once('includes/class-tgm-plugin-activation.php');
//require_once('includes/class-required-plugins.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */

//require_once('includes/wp_woocommerce_functions.php');

/* --------------------------------------------------------------
    ADD THEME SUPPORT
-------------------------------------------------------------- */

load_theme_textdomain( 'bylablum', get_template_directory() . '/languages' );
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio' ));
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );
add_theme_support( 'menus' );
add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form' ) );
add_theme_support( 'custom-background',
                  array(
                      'default-image' => '',    // background image default
                      'default-color' => '',    // background color default (dont add the #)
                      'wp-head-callback' => '_custom_background_cb',
                      'admin-head-callback' => '',
                      'admin-preview-callback' => ''
                  )
                 );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus( array(
    'header_menu' => __( 'Menu Header - Principal', 'bylablum' ),
    'home_menu' => __( 'Menu Home', 'bylablum' ),
    'footer_menu' => __( 'Menu Footer - Principal', 'bylablum' ),
) );

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action( 'widgets_init', 'bylablum_widgets_init' );
function bylablum_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Sidebar Principal', 'bylablum' ),
        'id' => 'main_sidebar',
        'description' => __( 'Estos widgets seran vistos en las entradas y páginas del sitio', 'bylablum' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebars( 4, array(
        'name'          => __('Footer Section %d'),
        'id'            => 'sidebar_footer',
        'description'   => 'Footer Section',
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ) );

    /* REGISTER THE CUSTOM WIDGETS */
    register_widget( 'bylablum_custom_recent_posts' );
    register_widget( 'bylablum_custom_mailchimp_form' );

}

/* --------------------------------------------------------------
    CUSTOM ADMIN LOGIN
-------------------------------------------------------------- */

function custom_admin_styles() {
    $version_remove = NULL;
    wp_register_style('wp-admin-style', get_template_directory_uri() . '/css/custom-wordpress-admin-style.css', false, $version_remove, 'all');
    wp_enqueue_style('wp-admin-style');
}
add_action('admin_init', 'custom_admin_styles');


function dashboard_footer() {
    echo '<span id="footer-thankyou">';
    _e ('Gracias por crear con ', 'bylablum' );
    echo '<a href="http://wordpress.org/" target="_blank">WordPress.</a> - ';
    _e ('Tema desarrollado por ', 'bylablum' );
    echo '<a href="http://robertochoa.com.ve/?utm_source=footer_admin&utm_medium=link&utm_content=bylablum" target="_blank">Screen Media Group</a></span>';
}
add_filter('admin_footer_text', 'dashboard_footer');

/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

require_once('includes/wp_custom_metabox.php');

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

require_once('includes/wp_custom_post_type.php');

/* --------------------------------------------------------------
    ADD CUSTOM THEME CONTROLS
-------------------------------------------------------------- */

require_once('includes/wp_custom_theme_control.php');

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if ( function_exists('add_theme_support') ) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size( 9999, 400, true);
}
if ( function_exists('add_image_size') ) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('blog_img', 276, 217, true);
    add_image_size('single_img', 636, 297, true );
}

/* --------------------------------------------------------------
    CUSTOM FUNCTION - ADD QUIZ ON HOME
-------------------------------------------------------------- */

function bylablum_quiz_generator($selector){
    $args = array('post_type' => 'quiz', 'posts_per_page' => -1, 'order' => 'DESC', 'orderby' => 'date');
    $quiz_container = new WP_Query($args);
    $i = 1;
    if ($quiz_container->have_posts()) :
    while ($quiz_container->have_posts()) : $quiz_container->the_post();
    $questions_array = (array)get_post_meta(get_the_ID(), 'blb_questions_selected', true);
    $quiz_id = get_the_ID();
    endwhile; ?>
<div action="" class="quiz-container quiz-container-hidden col-12">
    <form id="quiz-form" class="row align-items-start justify-content-center">
        <input type="hidden" name="quiz-id" value="<?php echo $quiz_id; ?>" />
        <?php foreach ($questions_array as $question_item) { ?>
        <?php if ($i == 1) { $class = 'active-quiz'; } else { $class = 'hidden-quiz'; } ?>
        <article id="quiz-<?php echo $i; ?>" class="quiz-content col-12 col-xl-5 col-lg-8 col-md-12 col-sm-12 animated fadeIn <?php echo $class; ?>">
            <?php $options = get_post($question_item); ?>
            <h4>
                <?php echo $options->post_content; ?>
            </h4>
            <?php $quiz_questions = get_post_meta($options->ID, 'preguntas_group', false); ?>
            <?php $quiz_questions = array_shift($quiz_questions); ?>
            <?php $y = 1; ?>
            <?php if (! empty ($quiz_questions)){ ?>
            <?php foreach ($quiz_questions as $item) { ?>
            <div class="radio-option">
                <input type="radio" class="radio-option-input" id="option_<?php echo $options->ID . '_' . $y; ?>" name="quiz-<?php echo $options->ID; ?>" value="<?php echo $item['blb_pregunta_score']; ?>" />
                <label for="option_<?php echo $options->ID . '_' . $y; ?>">
                    <?php echo $item['blb_pregunta_option']; ?></label>
            </div>
            <?php $y++; } } ?>
        </article>
        <?php $i++; } ?>
        <article id="quiz-mail" class="quiz-gray-content col-12 col-xl-5 col-lg-8 col-md-12 col-sm-12 animated fadeIn hidden-quiz">
            <h2>
                <?php _e('¡Coloca tu email para saber tu resultado!', 'bylablum'); ?>
            </h2>
            <p>
                <?php _e('Te enviaremos recursos útiles según tu perfil.')?>
            </p>
            <input type="text" id="name" name="nombre" class="form-control" placeholder="<?php _e('Nombre', 'bylablum'); ?>" />
            <small class="danger d-none"></small>
            <input type="email" id="email" name="correo" class="form-control" placeholder="<?php _e('E-mail', 'bylablum'); ?>" />
            <small class="danger d-none"></small>
            <button id="submit_quiz" type="submit" class="btn btn-md btn-green">
                <?php _e('Ver Mis Resultados', 'bylablum'); ?></button>
            <div class="quiz-mail-loader quiz-mail-loader-hidden">
                <div class="lds-ellipsis">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </article>
    </form>
    <div class="row align-items-start justify-content-center">
        <div class="quiz-error-handler quiz-error-handler-hidden col-12">
            <h3>
                <?php _e('Por favor seleccione una de las opciones', 'bylablum'); ?>
            </h3>
        </div>
        <div class="quiz-controller col-12 col-xl-5 col-lg-8 col-md-12 col-sm-12">
            <div class="row">
                <div class="quiz-button quiz-back quiz-button-hidden col-2">
                    <button id="quiz-back"><i class="fa fa-chevron-left"></i></button>
                </div>
                <div class="quiz-counter col-8">
                    <span>1</span><span>/
                        <?php echo count($questions_array); ?></span>
                </div>
                <div class="quiz-button quiz-next col-2">
                    <button id="quiz-next"><i class="fa fa-chevron-right"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    endif;
}

/* --------------------------------------------------------------
    CUSTOM FUNCTION - AJAX CALLER - QUIZ REPORTER
-------------------------------------------------------------- */

function bylablum_ajax_quiz_reporter() {
    $api_key = get_option('bylablum_mailchimp_api');

    /* UNSERIALIZE DATA */
    $info = array();
    parse_str($_POST['info'], $info);


    $acum_score = 0;
    foreach ($info as $key => $value) {
        if ((strpos($key, 'quiz-') === 0) && (strpos($key, 'quiz-id') !== 0)) {
            $acum_score = $acum_score + $value;
        }
    }

    $quiz_options = (array)get_post_meta($info['quiz-id'], 'quiz_options', true);
    foreach ($quiz_options as $quiz_options_item) {
        $quiz_score =  explode('-', $quiz_options_item['blb_quiz_range_score']);
        if (((int)$quiz_score[0] <= $acum_score) && ($acum_score <= (int)$quiz_score[1])) {
            $list_id = $quiz_options_item['blb_quiz_range_list'];
            $range_list = $quiz_options_item['blb_quiz_range_score'];
        }
    }

    $response = array();

    $nombre_largo = $info['nombre'];
    $correo = $info['correo'];

    $nombre = explode(' ', $nombre_largo);

    $server = 'us17.';

    $auth = base64_encode( 'user:'.$api_key );

    $data = array(
        'apikey'        => $api_key,
        'email_address' => $correo,
        'status'        => 'subscribed',
        'merge_fields'  => array(
            'FNAME' => $nombre[0],
            'LNAME'    => $nombre[1]
        )
    );

    $json_data = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://'.$server.'api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                                               'Authorization: Basic '.$auth));
    curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);

    $result = curl_exec($ch);

    $result_obj = json_decode($result);

    // printing the result obtained
    $status = $result_obj->status;

    if ($status == 'subscribed') {
        $response['message'] = __('Se ha añadido correctamente el correo a la lista, en breve le indicamos su puntuación', 'bylablum');
    }

    if ($status == 400) {
        $response['message'] = __('Ya su correo esta dentro de la lista, en breve le indicamos su puntuación', 'bylablum');
    }

    $response['url'] = home_url('/resultados/?quiz-id=' . $info['quiz-id'] . '&score=' . $acum_score );

    echo json_encode($response);
    die();
}


add_action('wp_ajax_nopriv_bylablum_ajax_quiz_reporter', 'bylablum_ajax_quiz_reporter');
add_action('wp_ajax_bylablum_ajax_quiz_reporter', 'bylablum_ajax_quiz_reporter');

/* --------------------------------------------------------------
    CUSTOM FUNCTION - AJAX CALLER - ADD SUBSCRIBER
-------------------------------------------------------------- */

function bylablum_custom_ajax_form_add_susbscriber_callback() {

    $api_key = get_option('bylablum_mailchimp_api');

    $info = array();
    parse_str($_POST['info'], $info);

    $list_id = $info['mailchimp-list'];

    $nombre_largo = $info['FNAME'];
    $correo = htmlentities($info['EMAIL']);

    $nombre = explode(' ', $nombre_largo);

    $server = 'us17.';

    $auth = base64_encode( 'user:'.$api_key );

    $data = array(
        'apikey'        => $api_key,
        'email_address' => $correo,
        'status'        => 'subscribed',
        'merge_fields'  => array(
            'FNAME' => $nombre[0],
            'LNAME'    => $nombre[1]
        )
    );

    $json_data = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://'.$server.'api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                                               'Authorization: Basic '.$auth));
    curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);

    $result = curl_exec($ch);

    $result_obj = json_decode($result);

    // printing the result obtained
    $status = $result_obj->status;

    if ($status == 'subscribed') {
        if (isset($info['file_download'])) {
            $response['message'] = __('Se ha añadido correctamente el correo a la lista, en breve iniciará su descarga', 'bylablum');
        } else {
            $response['message'] = __('Se ha añadido correctamente el correo a la lista', 'bylablum');
        }
    }

    if ($status == 400) {
        if (isset($info['file_download'])) {
            $response['message'] = __('Ya su correo esta dentro de la lista, en breve iniciará su descarga', 'bylablum');
        } else {
            $response['message'] = __('Ya su correo esta dentro de la lista', 'bylablum');
        }
    }

    echo json_encode($response);
    die();
}

add_action('wp_ajax_nopriv_bylablum_custom_ajax_form_add_susbscriber', 'bylablum_custom_ajax_form_add_susbscriber_callback');
add_action('wp_ajax_bylablum_custom_ajax_form_add_susbscriber', 'bylablum_custom_ajax_form_add_susbscriber_callback');
