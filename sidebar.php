<?php if ( is_active_sidebar( 'main_sidebar' ) ) : ?>
<ul id="sidebar" class="the-sidebar">
    <?php dynamic_sidebar( 'main_sidebar' ); ?>
</ul>
<?php endif; ?>

