<footer class="container-fluid" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row">
        <div class="the-footer col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row">
                    <div class="footer-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row">
                            <div class="footer-section col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="row">
                                    <div class="footer-item footer-menu col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                        <?php if ( is_active_sidebar( 'sidebar_footer' ) ) : ?>
                                        <ul id="sidebar-footer">
                                            <?php dynamic_sidebar( 'sidebar_footer' ); ?>
                                        </ul>
                                        <?php endif; ?>
                                    </div>
                                    <div class="footer-item col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                                        <?php if ( is_active_sidebar( 'sidebar_footer-2' ) ) : ?>
                                        <ul id="sidebar-footer">
                                            <?php dynamic_sidebar( 'sidebar_footer-2' ); ?>
                                        </ul>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="footer-section footer-newsletter col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="row">
                                    <div class="footer-item footer-newsletter-item col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                                        <?php if ( is_active_sidebar( 'sidebar_footer-3' ) ) : ?>
                                        <ul id="sidebar-footer">
                                            <?php dynamic_sidebar( 'sidebar_footer-3' ); ?>
                                        </ul>
                                        <?php endif; ?>
                                    </div>
                                    <div class="footer-item col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                        <?php if ( is_active_sidebar( 'sidebar_footer-4' ) ) : ?>
                                        <ul id="sidebar-footer">
                                            <?php dynamic_sidebar( 'sidebar_footer-4' ); ?>
                                        </ul>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="footer-instagram-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                            </div>
                            <div class="footer-copyright col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <h5>
                                    <?php _e('Copyright', 'bylablum'); ?> &copy; 2018 Anabelle Blum /
                                    <?php _e('DESARROLLADO POR SMG | AGENCIA DE MARKETING DIGITAL', 'bylablum'); ?>
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>

</html>
