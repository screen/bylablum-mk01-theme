<?php
/*  HOME SECTION - CUSTOM METABOX - HERO SECTION */
$cmb_home_hero = new_cmb2_box( array(
    'id'            => $prefix . 'home_hero_metabox',
    'title'         => esc_html__( 'Hero Principal', 'bylablum' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$cmb_home_hero->add_field( array(
    'name'         => __('Imagen Fondo del Hero', 'bylablum'),
    'desc'         => __( 'Imagen en JPG / PNG / BMP', 'bylablum'),
    'id'           => $prefix . 'hero_banner',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
));

$cmb_home_hero->add_field( array(
    'name'         => __('Logo del Hero', 'bylablum'),
    'desc'         => __( 'Imagen en PNG', 'bylablum'),
    'id'           =>  $prefix . 'hero_small_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
));

$cmb_home_hero->add_field( array(
    'name'         => __('Pequeño texto del Hero', 'bylablum'),
    'desc'         => __( 'Ingrese un texto corto que acompaña la imagen en PNG', 'bylablum'),
    'id'      => $prefix . 'hero_description',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
));

/*  HOME SECTION - CUSTOM METABOX - PODCAST SECTION */
$cmb_home_podcast = new_cmb2_box( array(
    'id'            => $prefix . 'home_podcast_metabox',
    'title'         => esc_html__( 'Sección: Podcast', 'bylablum' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$cmb_home_podcast->add_field( array(
    'name'         => __('¿Ocultar Podcast?', 'bylablum'),
    'desc'         => __( 'Seleccione esta opción si desea ocultar el Podcast', 'bylablum'),
    'id'           => $prefix . 'podcast_checkbox',
    'type'         => 'checkbox'
));

$cmb_home_podcast->add_field( array(
    'name'         => __('Imagen Fondo del Hero', 'bylablum'),
    'desc'         => __( 'Imagen en JPG / PNG / BMP', 'bylablum'),
    'id'           => $prefix . 'podcast_banner',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
));

$cmb_home_podcast->add_field( array(
    'name'         => __('Logo del Hero', 'bylablum'),
    'desc'         => __( 'Imagen en PNG', 'bylablum'),
    'id'           =>  $prefix . 'podcast_small_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
));

$cmb_home_podcast->add_field( array(
    'name'         => __('Primer Texto:', 'bylablum'),
    'desc'         => __( 'Ingrese el texto que va incialmente luego del banner', 'bylablum'),
    'id'      => $prefix . 'podcast_description1',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
));

$cmb_home_podcast->add_field( array(
    'name'         => __('Segundo Texto:', 'bylablum'),
    'desc'         => __( 'Ingrese el segundo texto que va incialmente luego del banner', 'bylablum'),
    'id'      => $prefix . 'podcast_description2',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
));

/*  HOME SECTION - CUSTOM METABOX - ABOUT SECTION */
$cmb_home_about = new_cmb2_box( array(
    'id'            => $prefix . 'home_about_metabox',
    'title'         => esc_html__( 'Sección: Acerca de Mi', 'bylablum' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$cmb_home_about->add_field( array(
    'name'         => __('Imagen Principal', 'bylablum'),
    'desc'         => __( 'Imagen en JPG / PNG / BMP', 'bylablum'),
    'id'           => $prefix . 'about_main_banner',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
));

$cmb_home_about->add_field( array(
    'name'         => __('Logo del About', 'bylablum'),
    'desc'         => __( 'Imagen en PNG', 'bylablum'),
    'id'           =>  $prefix . 'about_small_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
));

$cmb_home_about->add_field( array(
    'name'         => __('Contenido:', 'bylablum'),
    'desc'         => __( 'Ingrese el texto que describe esta sección', 'bylablum'),
    'id'      => $prefix . 'about_description',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
));

$cmb_home_about->add_field( array(
    'name'         => __('URL del Botón:', 'bylablum'),
    'desc'         => __( 'Ingrese la dirección URL que describe esta sección', 'bylablum'),
    'id'      => $prefix . 'about_btn_url',
    'type'    => 'text'
));

$cmb_home_about->add_field( array(
    'name'         => __('Texto del Botón:', 'bylablum'),
    'desc'         => __( 'Ingrese el texto del boton que describe esta sección', 'bylablum'),
    'id'      => $prefix . 'about_btn_text',
    'type'    => 'text'
));

/*  HOME SECTION - CUSTOM METABOX - QUIZ SECTION */
$cmb_home_quiz = new_cmb2_box( array(
    'id'            => $prefix . 'home_quiz_metabox',
    'title'         => esc_html__( 'Sección: Quiz', 'bylablum' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$cmb_home_quiz->add_field( array(
    'name'         => __('Logo del Quiz', 'bylablum'),
    'desc'         => __( 'Imagen en PNG', 'bylablum'),
    'id'           =>  $prefix . 'quiz_small_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
));

$cmb_home_quiz->add_field( array(
    'name'         => __('Quiz a Seleccionar:', 'bylablum'),
    'desc'         => __( 'Seleccione el quiz que se mostrará esta sección', 'bylablum'),
    'id'      => $prefix . 'quiz_selection',
    'type'    => 'pw_multiselect',
    'options' => $list_quiz,
));

$cmb_home_quiz->add_field( array(
    'name'         => __('Texto del Botón:', 'bylablum'),
    'desc'         => __( 'Ingrese el texto del boton que describe esta sección', 'bylablum'),
    'id'      => $prefix . 'quiz_btn_text',
    'type'    => 'text'
));

/*  HOME SECTION - CUSTOM METABOX - PROFILE SECTION */
$cmb_home_profile = new_cmb2_box( array(
    'id'            => $prefix . 'home_profile_metabox',
    'title'         => esc_html__( 'Sección: Perfiles', 'bylablum' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$group_field_id = $cmb_home_profile->add_field( [
    'id'      => $prefix . 'profile_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Perfil {#}', 'bylablum' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Perfil', 'bylablum' ),
        'remove_button' => __( 'Remover Perfil', 'bylablum' ),
        'sortable'      => true, // beta
        'closed'     => true, // true to have the groups closed by default
    ),
] );

$cmb_home_profile->add_group_field( $group_field_id, [
    'name'         => __('Imagen Principal', 'bylablum'),
    'desc'         => __( 'Imagen en JPG / PNG / BMP', 'bylablum'),
    'id'           => $prefix . 'profile_main_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
] );

$cmb_home_profile->add_group_field( $group_field_id, [
    'name'         => __('Logo del Perfil', 'bylablum'),
    'desc'         => __( 'Imagen en PNG', 'bylablum'),
    'id'           =>  $prefix . 'profile_small_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
] );

$cmb_home_profile->add_group_field( $group_field_id, [
    'name'         => __('Contenido:', 'bylablum'),
    'desc'         => __( 'Ingrese el texto que describe esta sección', 'bylablum'),
    'id'      => $prefix . 'profile_description',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
] );

$cmb_home_profile->add_group_field( $group_field_id, [
    'name'         => __('URL del Botón:', 'bylablum'),
    'desc'         => __( 'Ingrese la dirección URL que describe esta sección', 'bylablum'),
    'id'      => $prefix . 'profile_btn_url',
    'type' => 'text_url',
] );

$cmb_home_profile->add_group_field( $group_field_id, [
    'name'         => __('Texto del Botón:', 'bylablum'),
    'desc'         => __( 'Ingrese el texto del boton que describe esta sección', 'bylablum'),
    'id'      => $prefix . 'profile_btn_text',
    'type'    => 'text'
] );

/*  HOME SECTION - CUSTOM METABOX - LINKS SECTION */
$cmb_home_links = new_cmb2_box( array(
    'id'            => $prefix . 'home_links_metabox',
    'title'         => esc_html__( 'Sección: Enlaces RRSS / Especiales', 'bylablum' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$group_field_id = $cmb_home_links->add_field( [
    'id'      => $prefix . 'links_group',
    'type'    => 'group',
    'options'     => array(
        'group_title'   => __( 'Enlace {#}', 'bylablum' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Agregar Enlace', 'bylablum' ),
        'remove_button' => __( 'Remover Enlace', 'bylablum' ),
        'sortable'      => true, // beta
        'closed'     => true, // true to have the groups closed by default
    ),
] );

$cmb_home_links->add_group_field( $group_field_id, [
    'name'         => __('Logo del Enlace', 'bylablum'),
    'desc'         => __( 'Imagen en PNG', 'bylablum'),
    'id'           =>  $prefix . 'links_small_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
] );

$cmb_home_links->add_group_field( $group_field_id, [
    'name'         => __('Contenido:', 'bylablum'),
    'desc'         => __( 'Ingrese el texto que describe esta sección', 'bylablum'),
    'id'      => $prefix . 'links_description',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
] );

$cmb_home_links->add_group_field( $group_field_id, [
    'name'         => __('URL del Botón:', 'bylablum'),
    'desc'         => __( 'Ingrese la dirección URL que describe esta sección', 'bylablum'),
    'id'      => $prefix . 'links_btn_url',
    'type' => 'text_url',
] );

$cmb_home_links->add_group_field( $group_field_id, [
    'name'         => __('Texto del Botón:', 'bylablum'),
    'desc'         => __( 'Ingrese el texto del boton que describe esta sección', 'bylablum'),
    'id'      => $prefix . 'links_btn_text',
    'type'    => 'text'
] );
