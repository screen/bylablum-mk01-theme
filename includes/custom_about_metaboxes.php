<?php
/*  ABOUT SECTION - CUSTOM METABOX - HERO SECTION */
$cmb_about_hero = new_cmb2_box( array(
    'id'            => $prefix . 'about_hero_metabox',
    'title'         => esc_html__( 'Hero Principal', 'bylablum' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'       => array( 'key' => 'slug', 'value' => 'about' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$cmb_about_hero->add_field( array(
    'name'         => __('Imagen Fondo del Hero', 'bylablum'),
    'desc'         => __( 'Imagen en JPG / PNG / BMP', 'bylablum'),
    'id'           => $prefix . 'hero_banner',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
));

$cmb_about_hero->add_field( array(
    'name'         => __('Logo del Hero', 'bylablum'),
    'desc'         => __( 'Imagen en PNG', 'bylablum'),
    'id'           =>  $prefix . 'hero_small_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
));

$cmb_about_hero->add_field( array(
    'name'         => __('Pequeño texto del Hero', 'bylablum'),
    'desc'         => __( 'Ingrese un texto corto que acompaña la imagen en PNG', 'bylablum'),
    'id'      => $prefix . 'hero_description',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
));

/*  ABOUT SECTION - CUSTOM METABOX - INTRODUCTION SECTION */
$cmb_about_intro = new_cmb2_box( array(
    'id'            => $prefix . 'about_intro_metabox',
    'title'         => esc_html__( 'Sección: Introducción', 'bylablum' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'       => array( 'key' => 'slug', 'value' => 'about' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$cmb_about_intro->add_field( array(
    'name'         => __('Logo del Intro', 'bylablum'),
    'desc'         => __( 'Imagen en PNG', 'bylablum'),
    'id'           =>  $prefix . 'intro_small_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
));

$cmb_about_intro->add_field( array(
    'name'         => __('Título:', 'bylablum'),
    'desc'         => __( 'Ingrese el Título de esta seccion', 'bylablum'),
    'id'      => $prefix . 'intro_title',
    'type'    => 'text'
));

$cmb_about_intro->add_field( array(
    'name'         => __('Descripción:', 'bylablum'),
    'desc'         => __( 'Ingrese el texto que va incialmente luego del banner', 'bylablum'),
    'id'      => $prefix . 'intro_description',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
));

/*  ABOUT SECTION - CUSTOM METABOX - ABOUT SECTION */
$cmb_about_about = new_cmb2_box( array(
    'id'            => $prefix . 'about_about_metabox',
    'title'         => esc_html__( 'Sección: Acerca de Mi', 'bylablum' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'       => array( 'key' => 'slug', 'value' => 'about' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$cmb_about_about->add_field( array(
    'name'         => __('Imagen Principal', 'bylablum'),
    'desc'         => __( 'Imagen en JPG / PNG / BMP', 'bylablum'),
    'id'           => $prefix . 'about_main_banner',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
));

$cmb_about_about->add_field( array(
    'name'         => __('Logo del About', 'bylablum'),
    'desc'         => __( 'Imagen en PNG', 'bylablum'),
    'id'           =>  $prefix . 'about_small_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
));

$cmb_about_about->add_field( array(
    'name'         => __('Título:', 'bylablum'),
    'desc'         => __( 'Ingrese el Título de esta seccion', 'bylablum'),
    'id'      => $prefix . 'about_title',
    'type'    => 'text'
));

$cmb_about_about->add_field( array(
    'name'         => __('Contenido:', 'bylablum'),
    'desc'         => __( 'Ingrese el texto que describe esta sección', 'bylablum'),
    'id'      => $prefix . 'about_description',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
));

/*  ABOUT SECTION - CUSTOM METABOX - OBJECTIVE SECTION */
$cmb_about_quiz = new_cmb2_box( array(
    'id'            => $prefix . 'about_objective_metabox',
    'title'         => esc_html__( 'Sección: Objetivo', 'bylablum' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'       => array( 'key' => 'slug', 'value' => 'about' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    'closed'     => true, // true to keep the metabox closed by default
    'classes'    => 'extra-class', // Extra cmb2-wrap classes
    'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
) );

$cmb_about_quiz->add_field( array(
    'name'         => __('Logo del Objetivo', 'bylablum'),
    'desc'         => __( 'Imagen en PNG', 'bylablum'),
    'id'           =>  $prefix . 'objective_small_image',
    'type'         => 'file',
    'preview_size' => 'medium',
    'text'    => array(
        'add_upload_file_text' => __('Cargar Imagen', 'bylablum')
    ),
));

$cmb_about_quiz->add_field( array(
    'name'         => __('Descripción del Objetivo:', 'bylablum'),
    'desc'         => __( 'Ingrese el texto que va incialmente luego del banner', 'bylablum'),
    'id'      => $prefix . 'objective_description',
    'type'    => 'wysiwyg',
    'options' => [ 'textarea_rows' => 3 ]
));
