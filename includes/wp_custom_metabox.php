<?php
function ed_metabox_include_front_page( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'] ) ) {
        return $display;
    }

    if ( 'front-page' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return false;
    }

    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option( 'page_on_front' );

    // there is a front page set and we're on it!
    return $post_id == $front_page;
}
add_filter( 'cmb2_show_on', 'ed_metabox_include_front_page', 10, 2 );


function be_metabox_show_on_slug( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'], $meta_box['show_on']['value'] ) ) {
        return $display;
    }

    if ( 'slug' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return $display;
    }

    $slug = get_post( $post_id )->post_name;

    // See if there's a match
    return in_array( $slug, (array) $meta_box['show_on']['value']);
}
add_filter( 'cmb2_show_on', 'be_metabox_show_on_slug', 10, 2 );

add_action( 'cmb2_admin_init', 'bylablum_register_demo_metabox' );
function bylablum_register_demo_metabox() {
    $prefix = 'blb_';

    /* --------------------------------------------------------------
PREGUNTAS LIST
-------------------------------------------------------------- */
    $list_questions = array();
    $array_questions = new WP_Query(array('post_type' => 'preguntas', 'posts_per_page' => -1, 'order' => 'DESC', 'orderby' => 'date'));
    if ($array_questions->have_posts()) :
    while ($array_questions->have_posts()) : $array_questions->the_post();
    $list_questions[get_the_ID()] = get_the_title();
    endwhile;
    endif;

    /* --------------------------------------------------------------
QUIZ LIST
-------------------------------------------------------------- */
    $list_quiz = array();
    $array_quiz = new WP_Query(array('post_type' => 'quiz', 'posts_per_page' => -1, 'order' => 'DESC', 'orderby' => 'date'));
    if ($array_quiz->have_posts()) :
    while ($array_quiz->have_posts()) : $array_quiz->the_post();
    $list_quiz[get_the_ID()] = get_the_title();
    endwhile;
    endif;

    /* --------------------------------------------------------------
PAGES OPTIONS
-------------------------------------------------------------- */

    /* ---- HOME METABOXES ----- */

    require_once('custom_home_metaboxes.php');

    /* ---- ABOUT METABOXES ----- */

    require_once('custom_about_metaboxes.php');

    /* ---- QUESTIONS METABOXES ----- */


    $cmb_preguntas = new_cmb2_box( array(
        'id'            => $prefix . 'preguntas_metabox',
        'title'         => esc_html__( 'Opciones de la Pregunta', 'bylablum' ),
        'object_types'  => array( 'preguntas' ) // Post type
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
        // 'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
    ) );

    $preguntas_field_id = $cmb_preguntas->add_field( [
        'id'      => 'preguntas_group',
        'type'    => 'group',
        'options' => array
        (
            'group_title'   => __('Opción {#}', 'cmb2'), // {#} gets replaced by row number
            'add_button'    => __('Agrega otra opción', 'cmb2'),
            'remove_button' => __('Remover Opción', 'cmb2'),
            'sortable'      => true,
        ),
    ] );

    $cmb_preguntas->add_group_field( $preguntas_field_id, [
        'name'       => esc_html__( 'Opción / Opciones:', 'bylablum' ),
        'desc'       => esc_html__( 'Opción / Opciones de la Pregunta', 'bylablum' ),
        'id'         => $prefix . 'pregunta_option',
        'type'       => 'textarea_small',
        'classes' => 'cmb-custom-2columns',
        //'show_on_cb' => 'yourprefix_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'column'          => true, // Display field value in the admin post-listing columns
    ] );

    $cmb_preguntas->add_group_field( $preguntas_field_id, [
        'name'       => esc_html__( 'Puntuación:', 'bylablum' ),
        'desc'       => esc_html__( 'Puntuación de la Opción', 'bylablum' ),
        'id'         => $prefix . 'pregunta_score',
        'type'       => 'text_small',
        'classes' => 'cmb-custom-2columns',
        //'show_on_cb' => 'yourprefix_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only

        // 'column'          => true, // Display field value in the admin post-listing columns
    ] );


    /* ---- QUIZZES METABOXES ----- */

    $cmb_quizzes = new_cmb2_box( array(
        'id'            => $prefix . 'quiz_metabox',
        'title'         => esc_html__( 'Preguntas de Quiz', 'bylablum' ),
        'object_types'  => array( 'quiz' ) // Post type
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
        // 'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
    ) );

    $cmb_quizzes->add_field( array(
        'name'    => esc_html__( 'Seleccione Preguntas del Quiz:', 'bylablum' ),
        'id'      => $prefix . 'questions_selected',
        'desc'    => esc_html__( 'Seleccione las preguntas que correspondan con este quiz:', 'bylablum' ),
        'type'    => 'pw_multiselect',
        'options' => $list_questions,
        'attributes' => array(
            'placeholder' => esc_html__( 'Seleccione las preguntas. Haga Drag&Drop para reordenarlas.', 'bylablum' ),
        ),
    ) );

    $cmb_quizzes_options = new_cmb2_box( array(
        'id'            => $prefix . 'quiz_options',
        'title'         => esc_html__( 'Opciones de Quiz', 'bylablum' ),
        'object_types'  => array( 'quiz' ) // Post type
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
        // 'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
    ) );

    $options_field_id = $cmb_quizzes_options->add_field( [
        'id'      => 'quiz_options',
        'type'    => 'group',
        'options' => array
        (
            'group_title'   => __('Rango {#}', 'cmb2'), // {#} gets replaced by row number
            'add_button'    => __('Agrega otro rango', 'cmb2'),
            'remove_button' => __('Remover Rango', 'cmb2'),
            'sortable'      => false,
        ),
    ] );

    $cmb_quizzes_options->add_group_field( $options_field_id, [
        'name'       => esc_html__( 'Rango de Puntos:', 'bylablum' ),
        'desc'       => esc_html__( 'Inserte Rango de Puntos. E.G.: XX-XX', 'bylablum' ),
        'id'         => $prefix . 'quiz_range_score',
        'type'       => 'text_small',

        // 'classes' => 'cmb-custom-2columns',
        //'show_on_cb' => 'yourprefix_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only

        // 'column'          => true, // Display field value in the admin post-listing columns
    ] );

    $cmb_quizzes_options->add_group_field( $options_field_id, [
        'name'       => esc_html__( 'Descripción del Rango:', 'bylablum' ),
        'desc'       => esc_html__( 'Inserte Descripción del Rango', 'bylablum' ),
        'id'         => $prefix . 'quiz_range_desc',
        'type'       => 'wysiwyg',

        // 'classes' => 'cmb-custom-2columns',
        //'show_on_cb' => 'yourprefix_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only

        // 'column'          => true, // Display field value in the admin post-listing columns
    ] );

    $cmb_quizzes_options->add_group_field( $options_field_id, [
        'name'       => esc_html__( 'URL del botón en respuesta:', 'bylablum' ),
        'desc'       => esc_html__( 'Inserte URL del botón', 'bylablum' ),
        'id'         => $prefix . 'quiz_range_url',
        'type'       => 'text',
        'classes' => 'cmb-custom-2columns',
        // 'classes' => 'cmb-custom-2columns',
        //'show_on_cb' => 'yourprefix_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only

        // 'column'          => true, // Display field value in the admin post-listing columns
    ] );

    $cmb_quizzes_options->add_group_field( $options_field_id, [
        'name'       => esc_html__( 'Lista de Mailchimp:', 'bylablum' ),
        'desc'       => esc_html__( 'Inserte Lista de Mailchimp de Respuesta', 'bylablum' ),
        'id'         => $prefix . 'quiz_range_list',
        'type'       => 'text',
        'classes' => 'cmb-custom-2columns',
        // 'classes' => 'cmb-custom-2columns',
        //'show_on_cb' => 'yourprefix_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only

        // 'column'          => true, // Display field value in the admin post-listing columns
    ] );

    $cmb_post = new_cmb2_box( array(
        'id'            => $prefix . 'post_metabox',
        'title'         => esc_html__( 'Detalles Adicionales de la Nota', 'bylablum' ),
        'object_types'  => array( 'post' ) // Post type
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
        // 'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
    ) );

    $cmb_post->add_field( array(
         'name'       => esc_html__( 'Código de Publicidad:', 'bylablum' ),
        'desc'       => esc_html__( 'Inserte Código de Publicidad', 'bylablum' ),
        'id'         => $prefix . 'post_custom_code',
        'type'       => 'textarea',
        // 'classes' => 'cmb-custom-2columns',
        //'show_on_cb' => 'yourprefix_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'column'          => true, // Display field value in the admin post-listing columns
    ) );

}
