<?php
/* --------------------------------------------------------------
    CUSTOM GENERAL FUNCTIONS FOR THEME AND BOOTSTRAP
-------------------------------------------------------------- */
/* IMAGES RESPONSIVE ON ATTACHMENT IMAGES */
function add_image_class($class){
    $class .= ' img-fluid';
    return $class;
}
add_filter('get_image_tag_class','add_image_class', 10, 2);

/* ADD CONTENT WIDTH FUNCTION */
if ( ! isset( $content_width ) ) $content_width = 1170;

/* ADD ACTIVE CLASS TO NAV MENU */
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
    $classes[] = 'nav-item';
    if( in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }


    return $classes;
}

/* ADD PROPER CLASS TO BOOTSTRAP NAV MENU */
add_filter('wp_nav_menu','add_menuclass');
function add_menuclass($ulclass) {
    return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
}

/* FUNCTIONS FOR BREADCRUMBS */
function the_breadcrumb() {
?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item" aria-current="page">
            <a href="<?php echo home_url('/'); ?>" title="<?php _e('Volver al Home', 'bylablum'); ?>">Home</a>
        </li>
        <?php if ( !is_front_page() && is_home() ) { ?>
        <li class="breadcrumb-item"><a href="<?php echo home_url('/blog'); ?>" title="<?php _e('Blog by La Blum', 'bylablum'); ?>">
            <?php _e('Blog by La Blum', 'bylablum'); ?></a></li>
        <?php } else { ?>
        <li class="breadcrumb-item"><a href="<?php echo home_url('/blog'); ?>" title="<?php _e('Blog by La Blum', 'bylablum'); ?>">
            <?php _e('Blog by La Blum', 'bylablum'); ?></a></li>
        <?php } ?>
        <?php if (is_singular('post') ) { ?>
        <li class="breadcrumb-item active">
            <?php echo get_the_title(); ?>
        </li>
        <?php } ?>
    </ol>
</nav>
<?php }

/* --------------------------------------------------------------
    CUSTOM WIDGETS
-------------------------------------------------------------- */

/* CUSTOM RECENT POSTS - ORDERED BY NUMBER */
class bylablum_custom_recent_posts extends WP_Widget {

    /* CONSTRUCT */
    public function __construct() {
        $widget_ops = array(
            'classname' => 'custom_recent_posts',
            'description' => __('Este widget custom mostrará las entradas más populares', 'bylablum')
        );
        parent::__construct( 'bylablum_custom_recent_posts', 'Entradas más Populares', $widget_ops );
    }

    /* OUTPUT */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }
        $args2 = array('post_type' => 'post', 'posts_per_page' => $instance['quantity'], 'order' => 'DESC', 'orderby' => 'date');
        $custom_query = new WP_Query($args2);
        $i = 1;
        if ($custom_query->have_posts()) : ?>
<ul class="widget-post-list list-unstyled">
    <?php while ($custom_query->have_posts()) : $custom_query->the_post(); ?>
    <li class="media">
        <div class="media-number">
            <?php echo $i; ?>.
        </div>
        <div class="media-body">
            <a href="<?php the_permalink(); ?>" title="<?php _e('Abrir Entrada', 'bylablum'); ?>">
                <h5>
                    <?php the_title(); ?>
                </h5>
            </a>
        </div>
    </li>
    <?php $i++; endwhile; ?>
</ul>
<?php
        endif;
        wp_reset_query();
        echo $args['after_widget'];
    }

    /* FORM */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Más Populares', 'bylablum' );
        $quantity = ! empty( $instance['quantity'] ) ? $instance['quantity'] : 5; ?>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
        <?php esc_attr_e( 'Título:', 'bylablum' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
</p>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'quantity' ) ); ?>">
        <?php esc_attr_e( 'Cantidad de entradas:', 'bylablum' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'quantity' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'quantity' ) ); ?>" type="number" value="<?php echo esc_attr( $quantity ); ?>">
</p>
<?php
    }

    /* UPDATE */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
        $instance['quantity'] = ( ! empty( $new_instance['quantity'] ) ) ? sanitize_text_field( $new_instance['quantity'] ) : 5;

        return $instance;
    }
}

/* CUSTOM MAILCHIMP FORM  */
class bylablum_custom_mailchimp_form extends WP_Widget {

    public function bylablum_custom_mailchimp_form_scripts() {
        wp_enqueue_script( 'media-upload' );
        wp_enqueue_media();
        wp_enqueue_script('admin-functions', get_template_directory_uri() . '/js/admin-functions.js', array('jquery'), null, true);
    }

    /* CONSTRUCT */
    public function __construct() {
        $widget_ops = array(
            'classname' => 'custom_mailchimp_form',
            'description' => __('Este widget custom mostrará un formulario de Mailchimp personalizado, deberá agregarse la lista de Mailchimp al cual el user ingresará al momento de registrarse', 'bylablum')
        );
        parent::__construct( 'bylablum_custom_mailchimp_form', 'Formulario de Mailchimp', $widget_ops );

        add_action('admin_enqueue_scripts', array($this, 'bylablum_custom_mailchimp_form_scripts'));
    }

    /* OUTPUT */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        $mailchimp_list = ! empty( $instance['mailchimp_list'] ) ? $instance['mailchimp_list'] : '';
        $file_download = ! empty( $instance['file_download'] ) ? $instance['file_download'] : '';
        $unique_formid = uniqid('mailchimp_form_');
        $unique_id = substr($unique_formid, 15);
        $unique_buttonid = 'mailchimp_button_' . substr($unique_formid, 15);
?>
<form id="<?php echo $unique_formid; ?>" class="mailchimp_form" type="POST">
    <div class="custom-form-control-container">
        <input type="text" name="FNAME" id="FNAME" class="form-control" placeholder="<?php echo esc_html_e('Nombre', 'bylablum'); ?>" />
        <small class="danger d-none"></small>
        <input type="email" name="EMAIL" id="EMAIL" class="form-control" placeholder="<?php echo esc_html_e('Email', 'bylablum'); ?>" />
        <small class="danger d-none"></small>
        <input type="hidden" name="mailchimp-list" id="mailchimp-list" class="form-control" value="<?php echo $mailchimp_list; ?>" />
        <input type="hidden" name="file_download" id="file_download" class="form-control" value="<?php echo $file_download; ?>" />
    </div>
    <button id="<?php echo $unique_buttonid; ?>" class="btn btn-md btn-submit">
        <?php _e('¡Descargar Gratis Ya!', 'bylablum'); ?></button>
    <div id="response_<?php echo $unique_id; ?>" class="form-response"></div>
</form>

<?php
        echo $args['after_widget'];
    }

    /* FORM */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : '';
        $mailchimp_list = ! empty( $instance['mailchimp_list'] ) ? $instance['mailchimp_list'] : '';
        $file_download = ! empty( $instance['file_download'] ) ? $instance['file_download'] : '';
?>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
        <?php esc_attr_e( 'Título:', 'bylablum' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    <small>
        <?php _e('Título del Widget', 'bylablum'); ?></small>
</p>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'list' ) ); ?>">
        <?php esc_attr_e( 'Lista de Mailchimp:', 'bylablum' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'mailchimp_list' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'mailchimp_list' ) ); ?>" type="text" value="<?php echo esc_attr( $mailchimp_list ); ?>">
    <small>
        <?php _e('Aquí podrá agregar el ListID de Mailchimp a donde debe llegar el correo del suscriptor', 'bylablum'); ?></small>
</p>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'file_download' ) ); ?>">
        <?php esc_attr_e( 'Archivo a descargar al suscribirse: (Opcional)', 'bylablum' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'file_download' ); ?>" name="<?php echo $this->get_field_name( 'file_download' ); ?>" value="<?php echo esc_attr( $file_download ); ?>" />
    <small>
        <?php _e('Opcional: Agregue acá el archivo que el suscriptor ha ganado por suscribirse a Mailchimp', 'bylablum'); ?></small>

    <button class="upload_image_button button button-primary" style="display: block; margin-top: .3rem;">
        <?php _e('Cargar Archivo', 'bylablum'); ?></button>
</p>
<?php
    }

    /* UPDATE */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
        $instance['mailchimp_list'] = ( ! empty( $new_instance['mailchimp_list'] ) ) ? sanitize_text_field( $new_instance['mailchimp_list'] ) : '';
        $instance['file_download'] = ( ! empty( $new_instance['file_download'] ) ) ? esc_url_raw( $new_instance['file_download'] ) : '';
        return $instance;
    }
}
