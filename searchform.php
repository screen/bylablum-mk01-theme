<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
    <div class="input-group mb-3">
        <input type="search" class="form-control" id="s" name="s" value="" placeholder="<?php _e('BUSCAR','bylablum'); ?>"  aria-label="<?php _e('BUSCAR','bylablum'); ?>" aria-describedby="searchsubmit" />
        <div class="input-group-append">
            <button type="submit" class="btn btn-outline-secondary" id="searchsubmit"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>
