<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid main-container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php /* HERO SECTION */ ?>
        <section class="page-section section-hero col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row">
                    <div class="section-hero-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo get_post_meta(get_the_ID(), 'blb_hero_banner', true); ?>);">
                        <div class="container">
                            <div class="row row-hero align-items-center">
                                <div class="hero-content col-12 col-xl-7 col-lg-7 col-md-7 col-sm-8">
                                    <img src="<?php echo get_post_meta(get_the_ID(), 'blb_hero_small_image', true); ?>" alt="<?php echo get_bloginfo('name')?>" class="img-fluid" data-aos="fadeIn" data-aos-delay="200" />
                                    <div class="hero-text" data-aos="fadeIn" data-aos-delay="500">
                                        <?php $desc = get_post_meta( get_the_ID(), 'blb_hero_description', true ); ?>
                                        <?php echo apply_filters( 'the_content', $desc ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="row">
        <section class="page-section home-menu-section d-none d-sm-none d-md-none d-lg-flex d-xl-flex col-xl-12 col-lg-12 col-md-12">
            <div class="container-fluid home-menu-section-content">
                <div class="row align-items-center justify-content-center">
                    <div class="home-menu-logo col-xl-1 col-lg-1 col-md-1 col-sm-1 col-2 hidden">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="" class="img-fluid img-invert" />
                    </div>
                    <div class="home-menu-content col-xl-10 col-lg-10 col-md-10 col-sm-10 col-12">
                        <?php
                        wp_nav_menu( array(
                            'theme_location'    => 'home_menu',
                            'depth'             => 0, // 1 = with dropdowns, 0 = no dropdowns.
                            'container'         => 'div',
                            'menu_class'        => 'custom-home-nav'
                        ) );
                        ?>
                    </div>
                    <div class="home-menu-bar col-xl-1 col-lg-1 col-md-1 col-sm-1 col-2 hidden">
                        <div id="menu_icon" class="menu-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="home-menu-extra home-menu-extra-hidden">
                    <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'header_menu',
                        'container'         => 'div'
                    ) );
                    ?>
                </div>
            </div>

        </section>
    </div>
    <?php $show_podcast = get_post_meta( get_the_ID(), 'blb_podcast_checkbox', true ); ?>
    <?php if (!$show_podcast == 'on') { ?>
    <div class="row">
        <?php /* PODCAST SECTION */?>
        <section class="page-section section-podcast col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row">
                    <div class="section-podcast-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo get_post_meta(get_the_ID(), 'blb_podcast_banner', true); ?>);" data-aos="fadeIn" data-aos-delay="100">
                        <div class="container">
                            <div class="row row-podcast align-items-center">
                                <div class="podcast-hero col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
                                    <img src="<?php echo get_post_meta(get_the_ID(), 'blb_podcast_small_image', true); ?>" alt="<?php echo get_bloginfo('name')?>" class="img-fluid" data-aos="fadeIn" data-aos-delay="200" />
                                    <div class="podcast-button" data-aos="fadeIn" data-aos-delay="500">
                                        <a class="btn btn-md btn-gray" href="#">
                                            <?php _e('Escucha el Podcast', 'bylablum'); ?> <i class="fa fa-play"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-podcast-text-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row">
                            <div class="podcast-text-item podcast-text-item-first col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="250">
                                <?php $desc = get_post_meta( get_the_ID(), 'blb_podcast_description1', true ); ?>
                                <?php echo apply_filters( 'the_content', $desc ); ?>
                            </div>
                            <div class="podcast-text-item col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="300">
                                <?php $desc = get_post_meta( get_the_ID(), 'blb_podcast_description2', true ); ?>
                                <?php echo apply_filters( 'the_content', $desc ); ?>
                            </div>
                            <div class="podcast-text-item podcast-box-container col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="400">
                                <?php $args = array('post_type' => 'podcast', 'posts_per_page' => 6, 'orderby' => 'date', 'order' => 'DESC'); ?>
                                <?php $podcast_list = new WP_Query($args); ?>
                                <?php if ($podcast_list->have_posts()) : ?>
                                <ul class="podcast-box" data-aos="slideUp" data-aos-delay="200">
                                    <li class="podcast-heading">
                                        <h2>
                                            <?php _e('Escucha la lista de reproduccion', 'bylablum'); ?>
                                        </h2>
                                    </li>
                                    <?php while ($podcast_list->have_posts()) : $podcast_list->the_post(); ?>
                                    <li>
                                        <a href="<?php the_permalink(); ?>" title="<?php _e('Escuchar Podcast', 'bylablum'); ?>">
                                            <?php the_title(); ?> <i class="fa fa-play"></i></a>
                                    </li>
                                    <?php endwhile; ?>
                                </ul>
                                <?php endif; ?>
                                <?php wp_reset_query(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php } ?>
    <div class="row">
        <?php /* ABOUT SECTION */ ?>
        <section class="page-section section-about col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="section-about-item section-about-main-picture col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12" style="background: url(<?php echo get_post_meta(get_the_ID(), 'blb_about_main_banner', true); ?>);">

                    </div>
                    <div class="section-about-item col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="about-pre-heading">
                            <?php _e('Acerca de la blum', 'bylablum'); ?>
                        </div>
                        <div class="section-about-sprite-mobile d-block d-sm-block d-md-block d-lg-none d-lx-none">
                            <img src="<?php echo get_post_meta(get_the_ID(), 'blb_about_small_image', true); ?>" alt="<?php echo get_bloginfo('name')?>" class="img-fluid" data-aos="fadeIn" data-aos-delay="200" />
                        </div>
                        <div class="about-text-content">
                            <?php $desc = get_post_meta( get_the_ID(), 'blb_about_description', true ); ?>
                            <?php echo apply_filters( 'the_content', $desc ); ?>
                        </div>
                        <div class="section-about-button-mobile d-block d-sm-block d-md-block d-lg-none d-lx-none" data-aos="fadeIn" data-aos-delay="500">
                            <a class="btn btn-md btn-green" href="<?php echo get_post_meta(get_the_ID(), 'blb_about_btn_url', true); ?>" title="<?php echo get_post_meta(get_the_ID(), 'blb_about_btn_text', true); ?>">
                                <?php echo get_post_meta(get_the_ID(), 'blb_about_btn_text', true); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-about-sprite d-none d-sm-none d-md-none d-lg-block d-lx-block">
                <img src="<?php echo get_post_meta(get_the_ID(), 'blb_about_small_image', true); ?>" alt="<?php echo get_bloginfo('name')?>" class="img-fluid" data-aos="fadeIn" data-aos-delay="200" />
            </div>
            <div class="section-about-button d-none d-sm-none d-md-none d-lg-block d-lx-block" data-aos="fadeIn" data-aos-delay="500">
                <a class="btn btn-md btn-green" href="<?php echo get_post_meta(get_the_ID(), 'blb_about_btn_url', true); ?>" title="<?php echo get_post_meta(get_the_ID(), 'blb_about_btn_text', true); ?>">
                    <?php echo get_post_meta(get_the_ID(), 'blb_about_btn_text', true); ?></a>
            </div>
        </section>
    </div>
    <div class="row">
        <?php /* QUIZ SECTION */ ?>
        <div class="page-section quiz-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row">
                    <div class="section-quiz-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="section-quiz-content-first-content">
                            <img src="<?php echo get_post_meta(get_the_ID(), 'blb_quiz_small_image', true); ?>" alt="<?php echo get_bloginfo('name')?>" class="img-fluid img-quiz" data-aos="fadeIn" data-aos-delay="200" />
                            <div class="section-quiz-content-row row justify-content-center">
                                <div class="section-quiz-text col-xl-5 col-lg-8 col-md-10 col-sm-12 col-12">
                                    <?php $selected_quiz = (array)get_post_meta(get_the_ID(), 'blb_quiz_selection', true); ?>
                                    <?php $quiz_post = get_post($selected_quiz[0]); ?>
                                    <?php echo apply_filters( 'the_content', $quiz_post->post_content ); ?>
                                    <button id="startQuiz" class="btn btn-md btn-gray" id-quiz="<?php echo get_post_meta(get_the_ID(), 'blb_quiz_selection', true); ?>" title="<?php echo get_post_meta(get_the_ID(), 'blb_quiz_btn_text', true); ?>">
                                        <?php echo get_post_meta(get_the_ID(), 'blb_quiz_btn_text', true); ?></button>
                                </div>
                            </div>
                        </div>
                        <?php bylablum_quiz_generator('home'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php /* PROFILE SECTION */ ?>
        <?php wp_reset_query(); ?>
        <?php $features_group = (array)get_post_meta( get_the_ID(), 'blb_profile_group', true ); ?>
        <div class="page-section profile-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row">
                    <?php foreach ( $features_group as $feature ) { ?>
                    <div class="profile-item col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="500">
                        <img src="<?php echo $feature['blb_profile_main_image']; ?>" alt="<?php _e('Perfil', 'bylablum'); ?>" class="img-fluid" />
                        <div class="profile-item-text">
                            <img src="<?php echo $feature['blb_profile_small_image']; ?>" alt="<?php _e('Logo de Perfil', 'bylablum'); ?>" class="img-fluid img-profile-logo" />
                            <?php echo apply_filters( 'the_content', $feature['blb_profile_description'] ); ?>
                        </div>
                        <a class="btn btn-md btn-green" href="<?php echo esc_url($feature['blb_profile_btn_url']); ?>" target="_blank" title="<?php echo esc_html($feature['blb_profile_btn_text']); ?>">
                            <?php echo esc_html($feature['blb_profile_btn_text']); ?></a>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php /* LINKS SECTION */?>
        <?php $features_group = (array)get_post_meta( get_the_ID(), 'blb_links_group', true ); ?>
        <div class="page-section links-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row">
                    <div class="section-links-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="container">
                            <div class="row">
                                <?php foreach ( $features_group as $feature ) {?>
                                <div class="links-item col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="500">
                                    <img src="<?php echo $feature['blb_links_small_image']; ?>" alt="<?php _e('Perfil', 'bylablum'); ?>" class="img-fluid" />
                                    <div class="links-item-text">
                                        <?php echo apply_filters( 'the_content', $feature['blb_links_description'] ); ?>
                                    </div>
                                    <a class="btn btn-md btn-gray" href="<?php echo esc_url($feature['blb_links_btn_url']); ?>" title="<?php echo esc_html($feature['blb_links_btn_text']); ?>" target="_blank">
                                        <?php echo esc_html($feature['blb_links_btn_text']); ?></a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
